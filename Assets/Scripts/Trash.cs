﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{
    public float discardMinusPoint;

    public void DiscardVegetable(Vegetable vegetable)
    {
        //Here we can add some events when a vegetable is going to the trash.
    }
    public void DiscardSalad(List<Vegetable> salad)
    {
        //Here we can add some events when a salad is going to the trash.
    }
}
