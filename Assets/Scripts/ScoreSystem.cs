﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    [Header("Point Setup")]
    public int rightSaladPoint;
    public int leavingMinusPoint;
    public int trashMinusPoint;
    public int scorePickupPoint;

    int player1Score;
    int player2Score;

    public void AddSaladPointsToPlayer(int playerIndex)
    {
        if (playerIndex == 1)
            player1Score += rightSaladPoint;
        else if (playerIndex == 2)
            player2Score += rightSaladPoint;

        OnChangedScore(playerIndex);
    }

    public void MinusTrashPoint(int playerIndex)
    {
        if (playerIndex == 1)
            player1Score -= trashMinusPoint;
        else if (playerIndex == 2)
            player2Score -= trashMinusPoint;

        OnChangedScore(playerIndex);
    }
    public void MinusLeavingPoints(int playerIndex = 0)
    {
        if (playerIndex == 0)
        {
            player1Score -= leavingMinusPoint;
            OnChangedScore(1);
            player2Score -= leavingMinusPoint;
            OnChangedScore(2);
        }
        if (playerIndex == 1)
        {
            player1Score -= leavingMinusPoint;
            OnChangedScore(1);
        }
        else if (playerIndex == 2)
        {
            player2Score -= leavingMinusPoint;
            OnChangedScore(2);
        }
    }

    public void AddPickupPointsToPlayer(int playerIndex)
    {
        if (playerIndex == 1)
            player1Score += scorePickupPoint;
        else if (playerIndex == 2)
            player2Score += scorePickupPoint;

        OnChangedScore(playerIndex);
    }

    public (int winnerIndex, int winnerScore) GetWinner()
    {
        if (player1Score > player2Score)
            return (1, player1Score);
        else
            return (2, player2Score);
    }

    void OnChangedScore(int playerIndex)
    {
        if (playerIndex == 1)
            GameManager.Instance.uIManager.UpdateScoreText(playerIndex, player1Score);
        else if (playerIndex == 2)
            GameManager.Instance.uIManager.UpdateScoreText(playerIndex, player2Score);
    }

    public void SaveHighScore()
    {
        int highScoreIndex = -1;
        for (int i = 0; i < 10; i++)
        {
            if (GetWinner().winnerScore > PlayerPrefs.GetInt("HighScore" + i, 0))
            {
                highScoreIndex = i;
                break;
            }
        }
        if (highScoreIndex == -1)
                return; //Th score isn't in Top 10

        //Reorder Score List with New High Score
        for (int i = 9; i > highScoreIndex; i--)
            PlayerPrefs.SetInt("HighScore" + i, PlayerPrefs.GetInt("HighScore" + (i - 1).ToString()));
        PlayerPrefs.SetInt("HighScore" + highScoreIndex, GetWinner().winnerScore);
    }

    [ContextMenu("Clear Player Prefs")]
    public void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("PlayerPrefs is clear.");
    }
}
