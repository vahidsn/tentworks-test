﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("General Setup")]
    public float moveSpeed;
    public float choppingDuration;
    public int index;
    public float totalTime;

    [Header("Keys Setup")]
    public KeyCode moveUpKey;
    public KeyCode moveDownKey;
    public KeyCode moveLeftKey;
    public KeyCode moveRightKey;
    public KeyCode interactKey;

    [Header("Slots")]
    public SlotController slot1;
    public SlotController slot2;

    Table nearTable;
    ChoppingBoard nearChoppingBoard;
    Plate nearPlate;
    Trash nearTrash;
    Customer nearCustomer;
    Rigidbody myRigidbody;

    float remainingTime;
    float currentMoveSpeed;
    int verticalInput, horizontalInput;
    bool isChopping;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        remainingTime = totalTime;
        currentMoveSpeed = moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();

        remainingTime -= Time.deltaTime;
        GameManager.Instance.uIManager.UpdateTimeText(index, Mathf.Max(remainingTime, 0));
        if (remainingTime < 0)
        {
            GameManager.Instance.OnPlayerFinish();
            gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        myRigidbody.MovePosition(transform.position + new Vector3(horizontalInput, 0, verticalInput).normalized * currentMoveSpeed * Time.deltaTime);
    }

    void HandleInput()
    {
        if (isChopping)
            return;

        if (Input.GetKey(moveUpKey))
            verticalInput = 1;
        else if (Input.GetKey(moveDownKey))
            verticalInput = -1;
        else
            verticalInput = 0;

        if (Input.GetKey(moveRightKey))
            horizontalInput = 1;
        else if (Input.GetKey(moveLeftKey))
            horizontalInput = -1;
        else
            horizontalInput = 0;

        if (Input.GetKeyDown(interactKey))
            HandleInteract();
    }
    void HandleInteract()
    {
        if (nearTable)
        {
            HandleInteractWithTable();
        }
        else if (nearChoppingBoard)
        {
            HandleInteractWithChoppingBoard();
        }
        else if (nearPlate)
        {
            HandleInteractWithPlate();
        }
        else if (nearTrash)
        {
            HandleInteractWithTrash();
        }
        else if (nearCustomer && nearCustomer.enabled)
        {
            HandleInteractWithCustomer();
        }
    }

    #region Interaction
    void HandleInteractWithTable()
    {
        if (!slot1.HasVegetable && !slot1.HasSalad) //Check slot 1 is empty or not
        {
            slot1.AddVegetable(nearTable.GetVegetable());
        }
        else if (!slot2.HasVegetable && !slot2.HasSalad) //Check slot 1 is empty or not
        {
            slot2.AddVegetable(nearTable.GetVegetable());
        }
    }
    void HandleInteractWithChoppingBoard()
    {
        //Chopping
        if (slot1.HasVegetable && nearChoppingBoard.HasCapacity)
        {
            StartChopping();
            nearChoppingBoard.Chop(slot1.ReturnVegetable(), choppingDuration);
            Invoke("FinishChopping", choppingDuration);
            FillSlot1WithSlot2();
        }
        //Make Combination on Chopping Board
        else if (slot1.HasSalad && slot1.SaladAmount <= nearChoppingBoard.Capacity)
        {
            nearChoppingBoard.PlaceSalad(slot1.ReturnSalad());
            FillSlot1WithSlot2();
        }
        //Pick up Combination from Chopping Board
        else if (nearChoppingBoard.HasSalad)
        {
            if (!slot1.HasVegetable && nearChoppingBoard.SaladAmount <= slot1.Capacity)
            {
                slot1.AddSalad(nearChoppingBoard.PickUpSalad());
            }
            else if (!slot2.HasVegetable && nearChoppingBoard.SaladAmount <= slot2.Capacity)
            {
                slot2.AddSalad(nearChoppingBoard.PickUpSalad());
            }
        }
    }
    void HandleInteractWithPlate()
    {
        //Place vegetable
        if (slot1.HasVegetable && !nearPlate.HasVegetable && !nearPlate.HasSalad)
        {
            nearPlate.PlaceVegetable(slot1.ReturnVegetable());
            FillSlot1WithSlot2();
        }
        //Make Combination on the Plate
        else if (slot1.HasSalad && !nearPlate.HasVegetable && slot1.SaladAmount <= nearPlate.Capacity)
        {
            nearPlate.PlaceSalad(slot1.ReturnSalad());
            FillSlot1WithSlot2();
        }
        //Pick up Vegetable
        else if (nearPlate.HasVegetable)
        {
            if (!slot1.HasVegetable && !slot1.HasSalad)
            {
                slot1.AddVegetable(nearPlate.PickUpVegetable());
            }
            else if (!slot2.HasVegetable && !slot2.HasSalad)
            {
                slot2.AddVegetable(nearPlate.PickUpVegetable());
            }
        }
        //Pick up Salad
        else if (nearPlate.HasSalad)
        {
            if (!slot1.HasVegetable && nearPlate.SaladAmount <= slot1.Capacity)
            {
                slot1.AddSalad(nearPlate.PickUpSalad());
            }
            else if (!slot2.HasVegetable && nearPlate.SaladAmount <= slot2.Capacity)
            {
                slot2.AddSalad(nearPlate.PickUpSalad());
            }
        }
    }
    void HandleInteractWithTrash()
    {
        if (slot1.HasVegetable)
        {
            nearTrash.DiscardVegetable(slot1.ReturnVegetable());
            FillSlot1WithSlot2();
            GameManager.Instance.scoreSystem.MinusTrashPoint(index);
        }
        else if (slot1.HasSalad)
        {
            nearTrash.DiscardSalad(slot1.ReturnSalad());
            FillSlot1WithSlot2();
            GameManager.Instance.scoreSystem.MinusTrashPoint(index);
        }
    }
    void HandleInteractWithCustomer()
    {
        if (slot1.HasSalad)
        {
            nearCustomer.CheckSalad(slot1.ReturnSalad(), index);
            FillSlot1WithSlot2();
        }
    }
    #endregion

    void FillSlot1WithSlot2()
    {
        if (slot2.HasVegetable)
            slot1.AddVegetable(slot2.ReturnVegetable());
        else if (slot2.HasSalad)
            slot1.AddSalad(slot2.ReturnSalad());
    }

    void StartChopping()
    {
        isChopping = true;
    }
    void FinishChopping() //Call in Invoke - HandleIteractWithChoppingBoard()
    {
        isChopping = false;
    }

    //For Pickups
    public void AddTime(float amount)
    {
        remainingTime += amount;
    }
    public void ChangeSpeed(float amount)
    {
        currentMoveSpeed = moveSpeed + amount;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Table"))
        {
            nearTable = other.GetComponent<Table>();
        }
        else if (other.CompareTag("ChoppingBoard"))
        {
            nearChoppingBoard = other.GetComponent<ChoppingBoard>();
        }
        else if (other.CompareTag("Plate"))
        {
            nearPlate = other.GetComponent<Plate>();
        }
        else if (other.CompareTag("Trash"))
        {
            nearTrash = other.GetComponent<Trash>();
        }
        else if (other.CompareTag("Customer"))
        {
            nearCustomer = other.GetComponent<Customer>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Table"))
        {
            nearTable = null;
        }
        else if (other.CompareTag("ChoppingBoard"))
        {
            nearChoppingBoard = null;
        }
        else if (other.CompareTag("Plate"))
        {
            nearPlate = null;
        }
        else if (other.CompareTag("Trash"))
        {
            nearTrash = null;
        }
        else if (other.CompareTag("Customer"))
        {
            nearCustomer = null;
        }
    }
}
