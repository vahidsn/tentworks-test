﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vegetable : ScriptableObject
{
    public VegetableType type;
    public Sprite sprite;
}

public enum VegetableType { Broccoli, Cabbage, Carrot, Pepper, Spinach, Tomato }
