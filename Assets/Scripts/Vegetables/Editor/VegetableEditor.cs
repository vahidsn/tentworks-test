﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class VegetableEditor : Editor
{
    [MenuItem("Tools/Create Vegetable")]
    static void CreateVegetable()
    {
        string path = EditorUtility.SaveFilePanel("Create Vegetable", "Assets/Prefabs/Vegetables", "New Vegetable.asset", "asset");
        if (path == "")
            return;
        path = FileUtil.GetProjectRelativePath(path);

        Vegetable newSkill = CreateInstance<Vegetable>();
        AssetDatabase.CreateAsset(newSkill, path);
        AssetDatabase.SaveAssets();
    }
}
