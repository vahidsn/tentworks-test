﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChoppingBoard : MonoBehaviour
{
    public List<Vegetable> salad = new List<Vegetable>();
    public Transform saladUIParent;
    public TextMeshProUGUI timerText;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < saladUIParent.childCount; i++)
            saladUIParent.GetChild(i).gameObject.SetActive(false);
    }

    public void Chop(Vegetable vegetable, float choppingDuration)
    {
        StartCoroutine(ChoppingCoroutine(vegetable, choppingDuration));
    }
    IEnumerator ChoppingCoroutine(Vegetable vegetable, float choppingDuration)
    {
        float timer = choppingDuration;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            timerText.text = ((int)timer + 1).ToString();
            yield return null;
        }
        timerText.text = "";

        //UI Setup
        saladUIParent.GetChild(salad.Count).gameObject.SetActive(true);
        saladUIParent.GetChild(salad.Count).GetComponent<Image>().sprite = vegetable.sprite;

        salad.Add(vegetable);
    }
    public void PlaceSalad(List<Vegetable> newSalad)
    {
        if (salad.Count + newSalad.Count > 3)
            return;  //Not Enough Capacity

        //Combining two salads
        for (int i = 0; i < newSalad.Count; i++)
            salad.Add(newSalad[i]);
        for (int i = 0; i < salad.Count; i++)
        {
            saladUIParent.GetChild(i).gameObject.SetActive(true);
            saladUIParent.GetChild(i).GetComponent<Image>().sprite = salad[i].sprite;
        }
    }
    public List<Vegetable> PickUpSalad()
    {
        for (int i = 0; i < saladUIParent.childCount; i++)
            saladUIParent.GetChild(i).gameObject.SetActive(false);
        List<Vegetable> temp = new List<Vegetable>(salad);
        salad.Clear();
        return temp;
    }

    public int Capacity { get { return 3 - salad.Count; } }
    public int SaladAmount { get { return salad.Count; } }
    public bool HasCapacity { get { return salad.Count < 3; } }
    public bool HasSalad { get { return salad.Count > 0; } }
}
