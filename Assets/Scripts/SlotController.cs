﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotController : MonoBehaviour
{
    Vegetable vegetable;
    List<Vegetable> salad = new List<Vegetable>();

    List<Image> images = new List<Image>();

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            images.Add(transform.GetChild(i).GetComponent<Image>());
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void AddVegetable(Vegetable newVegetable)
    {
        if (vegetable == null && salad.Count == 0)
        {
            vegetable = newVegetable;
            //Handle UI
            images[0].gameObject.SetActive(true);
            images[0].sprite = newVegetable.sprite;
            images[0].color = Color.white;
        }
    }
    public Vegetable ReturnVegetable()
    {
        if (vegetable)
        {
            Vegetable temp = vegetable;
            vegetable = null;
            //Handle UI
            images[0].gameObject.SetActive(false);
            return temp;
        }
        else
        {
            return null;
        }
    }

    public void AddSalad(List<Vegetable> newSalad)
    {
        for (int i = 0; i < newSalad.Count; i++)
            salad.Add(newSalad[i]);

        //Handle UI
        for (int i = 0; i < salad.Count; i++)
        {
            images[i].gameObject.SetActive(true);
            images[i].sprite = salad[i].sprite;
            images[i].color = Color.yellow;
        }
    }
    public List<Vegetable> ReturnSalad()
    {
        //Handle UI
        for (int i = 0; i < images.Count; i++)
            images[i].gameObject.SetActive(false);

        List<Vegetable> temp = new List<Vegetable>(salad);
        salad.Clear();
        return temp;
    }

    public int SaladAmount { get { return salad.Count; } }
    public int Capacity { get { return 3 - salad.Count; } }
    public bool HasSalad { get { return salad.Count > 0; } }
    public bool HasVegetable { get { return vegetable != null; } }
}
