﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("Player 1 UI")]
    public TextMeshProUGUI player1TimeText;
    public TextMeshProUGUI player1ScoreText;

    [Header("Player 2 UI")]
    public TextMeshProUGUI player2TimeText;
    public TextMeshProUGUI player2ScoreText;

    [Header("End Panel UI")]
    public GameObject endPanel;
    public TextMeshProUGUI winnerNameText;
    public TextMeshProUGUI winnerScoreText;
    public Transform highScoresPanel;

    public void UpdateTimeText(int playerIndex, float time)
    {
        if (playerIndex == 1)
            player1TimeText.text = "Time: " + time.ToString("0.00");
        if (playerIndex == 2)
            player2TimeText.text = "Time: " + time.ToString("0.00");
    }
    public void UpdateScoreText(int playerIndex, int score)
    {
        if (playerIndex == 1)
            player1ScoreText.text = "Score: " + score;
        if (playerIndex == 2)
            player2ScoreText.text = "Score: " + score;
    }

    public void ShowEndPanel()
    {
        //Set winner texts
        int winnerIndex, winnerScore;
        (winnerIndex, winnerScore) = GameManager.Instance.scoreSystem.GetWinner();
        winnerNameText.text = "Player " + winnerIndex + " Wins!";
        winnerScoreText.text = "Score : " + winnerScore;

        //Set color texts base on winner color
        if (winnerIndex == 1)
            winnerNameText.color = winnerScoreText.color = Color.blue;
        else if(winnerIndex == 2)
            winnerNameText.color = winnerScoreText.color = Color.red;

        SetupHighScoresPanel();
        endPanel.SetActive(true);
    }

    void SetupHighScoresPanel()
    {
        GameManager.Instance.scoreSystem.SaveHighScore();
        for (int i = 0; i < 10; i++)
            highScoresPanel.GetChild(i).GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt("HighScore" + i).ToString();
    }
}
