﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimePickup : Pickup
{
    public float additionalTime;

    protected override void ApplyPickup()
    {
        base.ApplyPickup();
        targetPlayer.AddTime(additionalTime);
    }
}
