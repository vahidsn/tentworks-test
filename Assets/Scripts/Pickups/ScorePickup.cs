﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePickup : Pickup
{
    protected override void ApplyPickup()
    {
        base.ApplyPickup();
        GameManager.Instance.scoreSystem.AddPickupPointsToPlayer(targetPlayer.index);
    }
}
