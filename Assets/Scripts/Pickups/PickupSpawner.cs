﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour
{
    //Pickups gameobject refrence for instantiating
    public GameObject speedPickup;
    public GameObject timePickup;
    public GameObject scorePickup;

    [Header("Spawn Area")]
    [SerializeField] float minXPosition;
    [SerializeField] float maxXPosition;
    [SerializeField] float minZPosition;
    [SerializeField] float maxZPosition;
    [SerializeField] float YPosition;

    //Using pooling system for pickups gameobject creation
    Queue<GameObject> speedPickups = new Queue<GameObject>();
    Queue<GameObject> timePickups = new Queue<GameObject>();
    Queue<GameObject> scorePickups = new Queue<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        //Filling Pools
        for (int i = 0; i < 5; i++)
        {
            GameObject temp = Instantiate(speedPickup, transform);
            temp.SetActive(false);
            speedPickups.Enqueue(temp);

            temp = Instantiate(timePickup, transform);
            temp.SetActive(false);
            timePickups.Enqueue(temp);

            temp = Instantiate(scorePickup, transform);
            temp.SetActive(false);
            scorePickups.Enqueue(temp);
        }
    }

    public void SpawnPickup(int targetPlayerIndex)
    {
        GameObject spawnedObject = new GameObject();
        int randomTypeIndex = Random.Range(0, 3); //Random Pikcup Type: Speed, Time, Score
        switch (randomTypeIndex)
        {
            case 0:
                spawnedObject = speedPickups.Dequeue();
                break;
            case 1:
                spawnedObject = timePickups.Dequeue();
                break;
            case 2:
                spawnedObject = scorePickups.Dequeue();
                break;

        }
        spawnedObject.transform.position = new Vector3(Random.Range(minXPosition, maxXPosition), YPosition, Random.Range(minZPosition, maxZPosition));
        spawnedObject.GetComponent<Pickup>().Setup(targetPlayerIndex);
    }
    public void DestoryPickup(GameObject pickupGameobject)
    {
        pickupGameobject.SetActive(false);
        switch (pickupGameobject.GetComponent<Pickup>().type)
        {
            case PickupType.Speed:
                speedPickups.Enqueue(pickupGameobject);
                break;
            case PickupType.Time:
                timePickups.Enqueue(pickupGameobject);
                break;
            case PickupType.Score:
                scorePickups.Enqueue(pickupGameobject);
                break;
        }
    }
}

public enum PickupType { Speed, Time, Score }
