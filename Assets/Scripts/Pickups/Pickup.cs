﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Pickup : MonoBehaviour
{
    public PickupType type;

    protected PlayerController targetPlayer;
    
    int targetPlayerIndex;

    public virtual void Setup(int playerIndex)
    {
        targetPlayerIndex = playerIndex;
        if (playerIndex == 1)
            transform.GetChild(0).GetComponent<TextMeshPro>().color = Color.blue;
        else if(playerIndex == 2)
            transform.GetChild(0).GetComponent<TextMeshPro>().color = Color.red;
        gameObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.GetComponent<PlayerController>().index == targetPlayerIndex)
            {
                targetPlayer = other.GetComponent<PlayerController>(); //For using in the child classes
                ApplyPickup();
            }
        }
    }

    protected virtual void ApplyPickup()
    {
        GameManager.Instance.pickupSpawner.DestoryPickup(gameObject);
    }
}
