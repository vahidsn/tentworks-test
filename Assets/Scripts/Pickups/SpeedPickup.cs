﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPickup : Pickup
{
    public float additionalSpeed;
    public float duration;

    protected override void ApplyPickup()
    {
        targetPlayer.ChangeSpeed(additionalSpeed);
        Invoke("Finish", duration);
        base.ApplyPickup();
    }

    void Finish() //Called in Invoke - ApplyPickup()
    {
        targetPlayer.ChangeSpeed(-additionalSpeed);
    }
}
