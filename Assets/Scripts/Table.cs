﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : MonoBehaviour
{
    public Vegetable vegetableRefrence;

    public Vegetable GetVegetable()
    {
        Vegetable temp = ScriptableObject.CreateInstance("Vegetable") as Vegetable;
        temp.type = vegetableRefrence.type;
        temp.sprite = vegetableRefrence.sprite;

        return temp;
    }
}
