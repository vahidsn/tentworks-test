﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour
{
    public List<Vegetable> refrenceVegetables = new List<Vegetable>();
    [Tooltip("Delay between leaving and spawning again.")]
    public float spawnDelay;
    [Tooltip("This value is multiped with the number of vegetables in salad and set for waiting time.")]
    public float waitingTimeMultiplier;
    [Tooltip("This value is multiped with the WaitingTimeMultiplier when the customer is angry!")]
    public float angryTimerMultipier;

    [Header("UI Setup")]
    public Transform UISaladParent;
    public Image timeBar;

    List<Vegetable> requestedSalad = new List<Vegetable>();

    float waitingTime;
    float timer;
    float timerMultiplier = 1; //Using when a customer became angry!

    bool isSatisfied;
    bool isAngryFromPlayer1, isAngryFromPlayer2;

    // Start is called before the first frame update
    void Start()
    {
        Setup();
    }

    void Setup()
    {
        //Enable New Customer
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);
        enabled = true;

        //Reset Customer Setup
        isSatisfied = false;
        isAngryFromPlayer1 = isAngryFromPlayer2 = false;
        timerMultiplier = 1;
        timeBar.color = Color.white;

        //Set Requested Salad
        requestedSalad.Clear();
        int saladAmount = Random.Range(2, 4); //Random salad amount between 2 & 3
        for (int i = 0; i < saladAmount; i++)
        {
            int randomIndex = Random.Range(0, refrenceVegetables.Count);

            Vegetable temp = ScriptableObject.CreateInstance("Vegetable") as Vegetable;
            temp.type = refrenceVegetables[randomIndex].type;
            temp.sprite = refrenceVegetables[randomIndex].sprite;

            UISaladParent.GetChild(i).gameObject.SetActive(true);
            UISaladParent.GetChild(i).GetComponent<Image>().sprite = temp.sprite;

            requestedSalad.Add(temp);
        }

        //Set waiting time based on the number of vegetables in requested salad
        timer = waitingTime = requestedSalad.Count * waitingTimeMultiplier;
        StartCoroutine(WaitingCoroutine());
    }

    IEnumerator WaitingCoroutine()
    {
        while (timer > 0)
        {
            timer -= Time.deltaTime * timerMultiplier;
            timeBar.fillAmount = 1 - (waitingTime - timer) / waitingTime;
            yield return null;
        }

        Leave();
        yield return new WaitForSeconds(spawnDelay);
        Setup();
    }
    void Leave()
    {
        //Disable Customer
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);
        enabled = false;
        for (int i = 0; i < UISaladParent.childCount; i++)
            UISaladParent.GetChild(i).gameObject.SetActive(false);

        //Checking for penalizing players if the customer isn't satisfied
        if (!isSatisfied)
        {
            GameManager.Instance.scoreSystem.MinusLeavingPoints();
            if (isAngryFromPlayer1)
                GameManager.Instance.scoreSystem.MinusLeavingPoints(1);
            if (isAngryFromPlayer2)
                GameManager.Instance.scoreSystem.MinusLeavingPoints(2);
        }
    }

    public void CheckSalad(List<Vegetable> receivedSalad, int playerIndex)
    {
        isSatisfied = false;
        if (requestedSalad.Count == receivedSalad.Count) //Check that number of vegetables in received salad and requested salad are the same or not
        {
            //Check that combination of received salad and requested salad are the same or not.
            //If a vegetable in requested salad is the same with a vegetable in received sald, it removes from requested salad.
            //If all of the vegetables in the requested salad remove, it means two salads are the same.
            for (int i = 0; i < requestedSalad.Count; i++)
            {
                for (int j = 0; j < receivedSalad.Count; j++)
                {
                    if (requestedSalad[i].type == receivedSalad[j].type)
                    {
                        receivedSalad.RemoveAt(j);
                        break;
                    }
                }
            }
            if (receivedSalad.Count == 0)
            {
                isSatisfied = true;
                GameManager.Instance.scoreSystem.AddSaladPointsToPlayer(playerIndex);
                if (timer / waitingTime > 0.7f)
                    GameManager.Instance.pickupSpawner.SpawnPickup(playerIndex);
                timer = 0;
            }
        }
        //If received salad and requested salad are not the same, the customer will be angry!
        if (!isSatisfied)
        {
            if (playerIndex == 1)
                isAngryFromPlayer1 = true;
            if (playerIndex == 2)
                isAngryFromPlayer2 = true;
            MakeAngry();
        }
    }

    void MakeAngry()
    {
        timerMultiplier = angryTimerMultipier;
        timeBar.color = new Color(1, 0.3f, 0); //Orange
    }
}
