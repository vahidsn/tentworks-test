﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public ScoreSystem scoreSystem;
    public PickupSpawner pickupSpawner;
    public UIManager uIManager;

    int finishedPlayerCount;

    //Singleton Class
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    public void OnPlayerFinish()
    {
        finishedPlayerCount++;
        if (finishedPlayerCount == 2)
            uIManager.ShowEndPanel();
    }

    public void ResetGame()
    {
        SceneManager.LoadScene("MainScene");
    }
}