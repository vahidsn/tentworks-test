﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour
{
    public SlotController slot;

    public void PlaceVegetable(Vegetable newVegetable)
    {
        slot.AddVegetable(newVegetable);
    }
    public Vegetable PickUpVegetable()
    {
        return slot.ReturnVegetable();
    }

    public void PlaceSalad(List<Vegetable> newSalad)
    {
        slot.AddSalad(newSalad);
    }
    public List<Vegetable> PickUpSalad()
    {
        return slot.ReturnSalad();
    }

    public int SaladAmount { get { return slot.SaladAmount; } }
    public int Capacity { get { return 3 - slot.SaladAmount; } }
    public bool HasSalad { get { return slot.HasSalad; } }
    public bool HasVegetable { get { return slot.HasVegetable; } }
}
