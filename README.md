# Tentworks Interactive Test

This was a test project to apply for the Senior Unity Developer role in Tentworks Interactive. It took one week to be done and The goal was to create a “programmer-art” version of a salad chef simulation in Unity.
In this game two players compete against each other and the player who gains a higher scores wins the game when both player's times run out. The game saves top 10 scores in the high score list and shows them in the end panel. 
Each player has a specific color -> Player 1: Blue, Player 2: Red


Here is a description of the features done with C# in Unity. Note that the combination of vegetables are called a "Salad".


## Player Controller

Each player has these variables than can be different for each one:
- General variables: Move Speed, Chopping Duration, Player Index, Total Time
- Keys: Movement Keys and Interaction Key
- Slots: Each player has two slots as inventory for carrying vegetables and salads.

When a player enters an interactive environment object trigger it will store it and can interact with that object by pressing the interact key.
If it goes out from trigger it can't interact with that object anymore.

Each player has its own time that when it runs out, it will be deactivated and will call the GameManager to say the time is up.


## Vegetable

Vegetable class is a ScriptableObject object that has a type for recognition and a sprite for it's UI in the game. Also Salad is a List<Vegetable>.
We have 6 predefined vegetable types based on the test document in "Assets/Prefabs/Vegetables" path. We can create a vegetable object by "Tools/Create Vegetable" and assign its variable.


## Slot

Each slot can carry a vegetable OR a salad(3 vegetables max).


## Chopping Board

Each chopping board supports three action when a player interacts with it:
- Chopping a vegetable if the player slot1 has the vegetable.
- Placing a salad if the player slot1 has the salad and the chopping board is empty.
- Combining two salads if the player slot1 has the salad and the chopping board has a salad too. 


## Plate

Each plate supports three action when a player interacts with it:
- Placing a vegetable if the player slot1 has the vegetable and the plate is empty.
- Placing the salad if the player slot1 has the salad and the plate is empty.
- Combining two salads if the player slot1 has the salad and the plate has a salad too. 


## Trash

When a player enters the trash trigger and interacts with it, each item in the slot1 will be destroyed and the player gains a minus point.


## Customer

Each customer requests a salad and waits for it until a specific time. If it gets its requested salad from a player, it will award the player the defined score,
if the player delivers a wrong salad the customer will get angry and will penalize that player with a minus point if it leaves unsatisfied, if he doesn't get any salad at waiting time it will 
penalize both players with minus point and leaves.
The time bar of customers will be orange if they are angry.
When a customer leaves, after 1 second another customer will be spawned at the same positon.


## PickUp

We have 3 different pickups in the game that give good options to the their target player.:
- Speed: Increases the player’s movement speed for a period of time
- Time: Increases the overall time that the player has left 
- Score: Adds some points to the player score count


---

Developed by Vahid Siamaknejad (vahid.siamaknejad@gmail.com)
